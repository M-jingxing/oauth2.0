<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>已登录授权页面</title>

    <!--<link href="/view/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">-->
    <link href="/view/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/view/css/style.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="login"  id="user_loginPanel">
        <h1>${user.userName}<span class="loginSuccessTip">，已登录服务端</span></h1>
        <div class="authTip warning">请确认是否授权第三方获取您的基本信息</div>
        <button type="button" class="btn btn-primary btn-block-half btn-large"><i class="fa fa-arrow-left fa-fw"></i>
                &nbsp;&nbsp;&nbsp;取消</button>
        <button type="button" id="user_is_login_auth_btn" class="btn btn-primary btn-block-half btn-large" >确认授权&nbsp;&nbsp;&nbsp;<i
                class="fa fa-arrow-right fa-fw"></i></button>

</div>

<div class="login_success"  id="user_loginSuccessPanel"  style="display: none;">
    <h1><span id="user_login_name"></span>，登录并授权成功！</h1>
</div>
<script src="/view/js/jquery/jquery-1.11.1.min.js"></script>
<script src="/view/js/user_login_auth.js"></script>
</body>
</html>
