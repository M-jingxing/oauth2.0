( function() {
	// initEvent
    $("#web_register_btn").on("click", function () {
        var redirectUri = $("#web_redirectUri").val();

        $.ajax({
            type: "post",
            url: "/client/registerClient.do",
            data: {"redirectUri": redirectUri},
            dataType: "json",
            success: function(data){

                if(!data.success){
                    alert(data.errorMsg);
                    return;
                }

                var data = data.data;
                $("#register_redirectUri").text(data.redirectUri);
                $("#register_client_id").text(data.clientId);
                $("#register_client_secret").text(data.clientSecret);

                $("#web_registerPanel").hide();
                $("#web_registerSuccessPanel").show();

            }
        });

    });



}());