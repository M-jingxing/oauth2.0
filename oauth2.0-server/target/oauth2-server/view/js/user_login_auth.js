/**
 * Created by Administrator on 2017/4/12.
 */
( function() {
    
    // 登录按钮点击事件
    $("#user_login_auth_btn").on("click", function () {
        var user_id = $("#user_login_id").val();
        var password = $("#user_login_pwd").val();

        var user = {
            userId: user_id,
            password: password
        };

        $.ajax({
            type: "post",
            url: "/oauth/loginAuthorize.do",
            data: user,
            dataType: "json",
            success: function(data){

                if(!data.success){
                    alert(data.errorMsg);
                    return;
                }

                var data = data.data;
                $("#user_login_name").text(data.userName);

                $("#user_loginPanel").hide();
                $("#user_loginSuccessPanel").show();

            }
        });
    });

    // 登录按钮点击事件
    $("#user_is_login_auth_btn").on("click", function () {
        location.href = "/oauth/isLoginAuthorize.do";
    });

}());
