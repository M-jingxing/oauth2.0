/**
 * Created by Administrator on 2017/4/12.
 */
( function() {
    // initEvent
    // 用户注册按钮点击事件
    $("#user_register_btn").on("click", function () {
        var user_id = $("#user_id").val();
        var user_name = $("#user_name").val();
        var password = $("#password").val();
        var pwd = $("#pwd").val();
        var sex = $("#sex").val();
        var school = $("#school").val();
        var grade = $("#grade").val();

        if(password != pwd){
            alert("确认密码不一致！");
            return;
        }

        var user = {
            userId: user_id,
            userName: user_name,
            password: password,
            sex: sex,
            school: school,
            grade: grade
        };

        $.ajax({
            type: "post",
            url: "/user/registerUser.do",
            data: user,
            dataType: "json",
            success: function(data){

                if(!data.success){
                    alert(data.errorMsg);
                    return;
                }

                var data = data.data;
                $("#register_user_id").text(data.userId);
                $("#register_user_name").text(data.userName);
                $("#register_user_sex").text(data.sex == "1" ? "男" : "女");
                $("#register_user_school").text(data.school);
                $("#register_user_grade").text(data.grade);

                $("#user_registerPanel").hide();
                $("#user_registerSuccessPanel").show();

            }
        });
    });

    // 登录按钮点击事件
    $("#user_login_btn").on("click", function () {
        var user_id = $("#user_login_id").val();
        var password = $("#user_login_pwd").val();

        var user = {
            userId: user_id,
            password: password
        };

        $.ajax({
            type: "post",
            url: "/user/loginUser.do",
            data: user,
            dataType: "json",
            success: function(data){

                if(!data.success){
                    alert(data.errorMsg);
                    return;
                }

                var data = data.data;
                $("#user_login_name").text(data.userName);

                $("#user_loginPanel").hide();
                $("#user_loginSuccessPanel").show();

            }
        });
    });

    // 注销按钮点击事件
    $("#user_logout_btn").on("click", function () {

        $.ajax({
            type: "post",
            url: "/user/logoutUser.do",
            dataType: "json",
            success: function(data){

                if(!data.success){
                    alert(data.errorMsg);
                    return;
                }

                location.href = "user_login.html";
            }
        });
    });

    // 返回登录页面按钮点击事件
    $("#user_register_callbackBtn").on("click", function () {
        location.href = "user_login.html";
    });
    
}());
