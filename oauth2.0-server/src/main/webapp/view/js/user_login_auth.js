/**
 * Created by Administrator on 2017/4/12.
 */
( function() {
    
    // 使用其它账号按钮点击事件
    $("#user_auth_otherBtn").on("click", function () {
        location.href = "user_login_auth.html";
    });

    // 确认授权按钮点击事件
    $("#user_is_login_auth_btn").on("click", function () {
        location.href = "/oauth/isLoginAuthorize.do";
    });

}());
