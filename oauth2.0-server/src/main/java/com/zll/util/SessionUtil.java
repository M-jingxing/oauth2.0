package com.zll.util;

import com.zll.domain.User;

import javax.servlet.http.HttpSession;

/**
 * Created by Administrator on 2017/4/13.
 */
public class SessionUtil {

    public static void setSessionUser(HttpSession session, User user){

        session.setAttribute("currentUser",user);
    }

    public static User getSessionUser(HttpSession session){
        User user = (User) session.getAttribute("currentUser");
        return user;
    }

    public static Boolean removeSessionUser(HttpSession session){
        session.removeAttribute("currentUser");
        return true;
    }

    public static void setSessionClientId(HttpSession session,String client_id){
        session.setAttribute("client_id",client_id);
    }

    public static String getSessionClientId(HttpSession session){
        String clientId = (String) session.getAttribute("client_id");
        return clientId;
    }

}
