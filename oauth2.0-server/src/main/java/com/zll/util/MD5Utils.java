/** 
 *功能说明：字符串助手类 
 */
package com.zll.util;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.util.Random;


public class MD5Utils {
	private static BASE64Encoder encoder;
	private static BASE64Decoder decoder;
	
	public static boolean isNULLorEmpty(String str){
	  if(str==null||str.trim().equals(""))
		return true;
	  else
		return false;
	}
	
	/**
	 * 
	 * 方法名称：encode
	 * 功能说明：字符串数据进行BASE64加密
	 * @param str
	 * @return
	 */
	public static String encode(String str){
		if(encoder == null) encoder = new BASE64Encoder();
		return encoder.encode(str.getBytes());
	}
	
	/**
	 * 
	 * 方法名称：decode
	 * 功能说明：字符串数据进行BASE64解密
	 * @param str
	 * @return
	 * @throws IOException
	 */
	public static String decode(String str) throws IOException {
		if(decoder==null) decoder = new BASE64Decoder();
		return new String(decoder.decodeBuffer(str));
	}
	
	/**
	 * 
	 * 方法名称：truncate
	 * 功能说明：根据指定长度截取字符串
	 * @param str
	 * @param length
	 * @return
	 */
	public static String truncate(String str, int length){
	   if(str != null && str.length()> length){
	      str = str.substring(0,length)+"...";
	   }
	   return str;
	}
	
	/**
	 * 先去掉字符串中的单引号，再根据指定长度截取字符串
	 */
	public static String truncateDetail(String value, int length){
		String str=value.replaceAll("'","");
		if(str != null && str.length()> length){
			str = str.substring(0,length)+"...";
		}
		return str;
	}
	
	/**
	 * 字符串数据进行MD5算法加密
	 */
	public static String MD5(String s) {
		String resultStr = null;

		try {
			byte[] source = s.getBytes("UTF-8");
			char hexDigits[] = { // 用来将字节转换成 16 进制表示的字符
			'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd',
				'e', 'f' };
		
			java.security.MessageDigest md = java.security.MessageDigest
					.getInstance("MD5");
			md.update(source);
			byte tmp[] = md.digest(); // MD5 的计算结果是一个 128 位的长整数，
										// 用字节表示就是 16 个字节
			char str[] = new char[16 * 2]; // 每个字节用 16 进制表示的话，使用两个字符，
											// 所以表示成 16 进制需要 32 个字符
			int k = 0; // 表示转换结果中对应的字符位置
			for (int i = 0; i < 16; i++) { // 从第一个字节开始，对 MD5 的每一个字节
											// 转换成 16 进制字符的转换
				byte byte0 = tmp[i]; // 取第 i 个字节
				str[k++] = hexDigits[byte0 >>> 4 & 0xf]; // 取字节中高 4 位的数字转换,
															// >>>
															// 为逻辑右移，将符号位一起右移
				str[k++] = hexDigits[byte0 & 0xf]; // 取字节中低 4 位的数字转换
			}
			resultStr = new String(str); // 换后的结果转换为字符串

		} catch (Exception e) {
			e.printStackTrace();
		}

		return resultStr;
	}
    
    
	/**
	 * 获取随机整数(0~9).
	 * @param  随机整数的长度
	 * @return 随机整数
	 */
	public static String getRandomInt(int len) {
		Random random = new Random();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < len; i++) {
			int c = random.nextInt(10);
			String strRand= Integer.toString(c);
			sb.append(strRand);
		}
		return sb.toString();
	}
	
	/**
	 * 获取随机数(0~9,a~z).
	 * @param  随机数的长度
	 * @return 随机数
	 */
	public static String getRandomStr(int len) {
		Random random = new Random();
		StringBuffer sb = new StringBuffer();
		String strRand = null;
		for (int i = 0; i < len; i++) {
			// 得到随机产生的验证码数字
			while (i >= 0) {
				int c = random.nextInt(123);
				if ((c <= '9' && c >= '0') ||(c <= 'z' && c >= 'a')) {
					strRand = String.valueOf((char) c);
					break;
				}
			}
			// 产生四个随机数字组合在一起
			sb.append(strRand);
		}
		return sb.toString();
	}
	
	public static void main(String[] args) throws IOException {
		String str = getRandomStr(6);
		System.out.println(str);
		String strEn = MD5Utils.encode(str);
		System.out.println(strEn);
		String strDe = MD5Utils.decode(strEn);
		System.out.println(strDe);
	}
   
}
