package com.zll.dao;

import com.zll.domain.AuthInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2017/4/12.
 */
@Repository("AuthInfoMapper")
public interface AuthInfoMapper {
    AuthInfo selectByPrimaryKey(Integer id);
    Integer insert(AuthInfo authInfo);
    AuthInfo selectByClientIdAndUserId(@Param("clientId")String clientId, @Param("userId") String userId);
    AuthInfo selectByClientIdAndCode(@Param("clientId")String clientId, @Param("code") String code);
    AuthInfo selectByAccessToken(String accessToken);
    Integer updateByPrimaryKey(AuthInfo authInfo);
}
