package com.zll.dao;

import com.zll.domain.User;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2017/4/11.
 */
@Repository("UserMapper")
public interface UserMapper {
    User selectByPrimaryKey(Long id);
    User selectByUserId(String userId);
    Integer insert(User user);

}
