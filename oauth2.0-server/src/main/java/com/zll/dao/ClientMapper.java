package com.zll.dao;

import com.zll.domain.Client;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


/**
 * Created by Administrator on 2017/4/11.
 */
@Repository("ClientMapper")
public interface ClientMapper {
    Client selectByClientId(String clientId);
    Integer insert(Client client);
    Client selectByRedirectUri(String redirectUri);
    Client selectByClientIdAndRedirectUri(@Param("clientId") String clientId, @Param("redirectUri") String redirectUri);
}
