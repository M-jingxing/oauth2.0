package com.zll.service;

import com.zll.domain.AuthInfo;
import com.zll.domain.User;

import java.util.Map;

/**
 * Created by Administrator on 2017/4/13.
 */
public interface IAuthInfoService {
    AuthInfo selectByPrimaryKey(Integer id);
    Boolean insertAuthInfo(AuthInfo authInfo);
    AuthInfo selectByClientIdAdnUserId(String clientId,String userId);
    AuthInfo selectByClientIdAndCode(String clientId,String code);
    AuthInfo selectByAccessToken(String accessToken);
    Boolean updateByPrimaryKey(AuthInfo authInfo);
    String getCode();
    Boolean isCodeValid(String client_id,String userId);
    String getOldCodeOrNewCode(String client_id, String userId);
    String getOldTokenOrNewToken(String client_id,String userId);
    Boolean sendCodeToClient(String code, String client_id);

}
