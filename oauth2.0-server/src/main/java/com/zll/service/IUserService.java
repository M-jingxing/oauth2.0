package com.zll.service;

import com.zll.domain.User;

/**
 * Created by Administrator on 2017/4/12.
 */
public interface IUserService {

    User selectByPrimaryKey(Long id);
    User selectByUserId(String userId);
    Boolean insertUserInfo(User user);

}
