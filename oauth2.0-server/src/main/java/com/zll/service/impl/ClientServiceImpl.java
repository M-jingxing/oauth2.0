package com.zll.service.impl;

import com.zll.dao.ClientMapper;
import com.zll.domain.Client;
import com.zll.service.IClientService;
import com.zll.util.MD5Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * Created by Administrator on 2017/4/12.
 */
@Service("ClientServiceImpl")
@Transactional(rollbackFor = Exception.class)
public class ClientServiceImpl implements IClientService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ClientMapper clientMapper;



    @Override
    public Boolean insertClientInfo(Client client) {

        String redirectUri = client.getRedirectUri();
        if (StringUtils.isEmpty(redirectUri)){
            return false;
        }

        //encrypt clientId and clientSecret
        String clientId = MD5Utils.encode(redirectUri);
        String createDate = new Date().toString();
        String clientSecret = MD5Utils.encode(createDate);

        //save info
        client.setClientId(clientId);
        client.setClientSecret(clientSecret);
        client.setCreateTime(createDate);
        client.setUpdateTime(createDate);

        //insert to db
        int result = clientMapper.insert(client);
        logger.info("result:"+result);

        return  result > 0;
    }

    @Override
    public Client selectByClientId(String clientId) {
        return clientMapper.selectByClientId(clientId);
    }

    @Override
    public Client selectByRedirectUri(String redirectUri) {
        return clientMapper.selectByRedirectUri(redirectUri);
    }

    @Override
    public Client selectByClientIdAndRedirectUri(String clientId, String redirectUri) {
        return clientMapper.selectByClientIdAndRedirectUri(clientId,redirectUri);
    }
}
