package com.zll.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.mysql.jdbc.StringUtils;
import com.zll.dao.AuthInfoMapper;
import com.zll.dao.ClientMapper;
import com.zll.domain.AuthInfo;
import com.zll.domain.Client;
import com.zll.enums.FlagEnum;
import com.zll.service.IAuthInfoService;
import com.zll.util.HttpClientImitator;
import com.zll.util.HttpClientUtil;
import com.zll.util.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.util.calendar.LocalGregorianCalendar;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/4/13.
 */
@Service("AuthInfoServiceImpl")
public class AuthInfoServiceImpl implements IAuthInfoService{

    @Autowired
    private AuthInfoMapper authInfoMapper;
    @Autowired
    private ClientMapper clientMapper;

    @Override
    public AuthInfo selectByPrimaryKey(Integer id) {
        return authInfoMapper.selectByPrimaryKey(id);
    }

    @Override
    public Boolean insertAuthInfo(AuthInfo authInfo) {
        return authInfoMapper.insert(authInfo) > 0;
    }

    @Override
    public AuthInfo selectByClientIdAdnUserId(String clientId,String userId) {
        return authInfoMapper.selectByClientIdAndUserId(clientId,userId);
    }

    @Override
    public AuthInfo selectByClientIdAndCode(String clientId, String code) {
        return authInfoMapper.selectByClientIdAndCode(clientId,code);
    }

    @Override
    public AuthInfo selectByAccessToken(String accessToken) {
        return authInfoMapper.selectByAccessToken(accessToken);
    }

    @Override
    public Boolean updateByPrimaryKey(AuthInfo authInfo) {
        return authInfoMapper.updateByPrimaryKey(authInfo) > 0;
    }

    @Override
    public String getCode() {
        String code = MD5Utils.getRandomStr(6);
        String encryptCode = MD5Utils.encode(code);

        return encryptCode;
    }

    @Override
    public Boolean isCodeValid(String client_id,String userId) {
        if (StringUtils.isNullOrEmpty(client_id) || StringUtils.isNullOrEmpty(userId)){
            return false;
        }
        AuthInfo authInfo = authInfoMapper.selectByClientIdAndUserId(client_id,userId);
        if (authInfo == null){
            return false;
        }
        if (StringUtils.isNullOrEmpty(authInfo.getCode())){
            return false;
        }
        if (!authInfo.getCodeFlag().equals(FlagEnum.EFFICACY.getKey())){
            return false;
        }
        return true;
    }

    @Override
    public String getOldCodeOrNewCode(String client_id, String userId) {
        AuthInfo oldAuthInfo = authInfoMapper.selectByClientIdAndUserId(client_id,userId);
        if (oldAuthInfo == null){
            AuthInfo authInfo = new AuthInfo();
            String code = getCode();
            authInfo.setClientId(client_id);
            authInfo.setUserId(userId);
            authInfo.setCode(code);
            authInfo.setCodeFlag(FlagEnum.EFFICACY.getKey());
            authInfoMapper.insert(authInfo);

            return code;
        }

        if (!oldAuthInfo.getCodeFlag().equals(FlagEnum.EFFICACY.getKey())) {
            String code2 = getCode();
            oldAuthInfo.setCode(code2);
            oldAuthInfo.setCodeFlag(FlagEnum.EFFICACY.getKey());
            authInfoMapper.updateByPrimaryKey(oldAuthInfo);

            return code2;
        }

        return oldAuthInfo.getCode();
    }

    @Override
    public String getOldTokenOrNewToken(String client_id,String userId) {

        AuthInfo authInfo = authInfoMapper.selectByClientIdAndUserId(client_id,userId);

        if (StringUtils.isNullOrEmpty(authInfo.getAccessToken())
                || !FlagEnum.EFFICACY.getKey().equals(authInfo.getTokenFlag())){
            String token = getToken();
            authInfo.setAccessToken(token);
            authInfo.setTokenFlag(FlagEnum.EFFICACY.getKey());
            authInfoMapper.updateByPrimaryKey(authInfo);

            return token;
        }

        return authInfo.getAccessToken();
    }

    @Override
    public Boolean sendCodeToClient(String code, String client_id) {
        Client client = clientMapper.selectByClientId(client_id);
        if(client == null) {
            return false;
        }
        if (StringUtils.isNullOrEmpty(client.getRedirectUri())){
            return false;
        }
        String requestUri = client.getRedirectUri() + "?code=" + code;
        JSONObject result = HttpClientUtil.httpGet(requestUri);
        System.out.println("result:"+result.toString());
        return true;
    }

    private String getToken(){
        return MD5Utils.getRandomStr(10);
    }

}
