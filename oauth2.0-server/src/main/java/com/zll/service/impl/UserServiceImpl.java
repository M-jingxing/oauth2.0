package com.zll.service.impl;

import com.zll.dao.UserMapper;
import com.zll.domain.User;
import com.zll.service.IUserService;
import com.zll.util.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2017/4/12.
 */
@Service("UserServiceImpl")
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User selectByPrimaryKey(Long id) {
        return userMapper.selectByPrimaryKey(id);
    }

    @Override
    public User selectByUserId(String userId) {
        return userMapper.selectByUserId(userId);
    }

    @Override
    public Boolean insertUserInfo(User user) {
        String encryptPassword = MD5Utils.encode(user.getPassword());
        user.setPassword(encryptPassword);
        return userMapper.insert(user) > 0;
    }
}
