package com.zll.service;

import com.zll.domain.Client;

/**
 * Created by Administrator on 2017/4/12.
 */
public interface IClientService {
    Boolean insertClientInfo(Client client);
    Client selectByClientId(String clientId);
    Client selectByRedirectUri(String redirectUri);
    Client selectByClientIdAndRedirectUri( String clientId, String redirectUri);

}
