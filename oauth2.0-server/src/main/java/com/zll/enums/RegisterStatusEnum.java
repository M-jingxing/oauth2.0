package com.zll.enums;

/**
 * Created by Administrator on 2017/4/12.
 */
public enum RegisterStatusEnum {
     SUCCESS("0","success"),
     FAILURE("1","failure"),
     PARAM_EMPTY("2","param empty"),
     ALREADY_EXIST("3","already exist");

     private String key;
     private String value;
     RegisterStatusEnum(String key, String value){
          this.key = key;
          this.value = value;
     }

     public String getKey() {
          return key;
     }

     public void setKey(String key) {
          this.key = key;
     }

     public String getValue() {
          return value;
     }

     public void setValue(String value) {
          this.value = value;
     }

     public static String getRegisterStatusMsg(String key) {
          for (RegisterStatusEnum statusEnum:RegisterStatusEnum.values()) {
               if (key.equals(statusEnum.getKey())){
                    return  statusEnum.getValue();
               }
          }
          return "";
     }
}
