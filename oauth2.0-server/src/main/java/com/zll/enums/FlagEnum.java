package com.zll.enums;

/**
 * Created by Administrator on 2017/4/12.
 */
public enum FlagEnum {

    LOSE_EFFICACY("0","lose efficacy"),
    EFFICACY("1","efficacy");

    private String key;
    private String value;

    FlagEnum(String key, String value){
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String getFlagMsg(String key) {
        for (FlagEnum statusEnum: FlagEnum.values()) {
            if (key.equals(statusEnum.getKey())){
                return  statusEnum.getValue();
            }
        }
        return "";
    }
}
