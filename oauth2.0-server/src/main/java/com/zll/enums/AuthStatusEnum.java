package com.zll.enums;

/**
 * Created by Administrator on 2017/4/12.
 */
public enum AuthStatusEnum {

    SUCCESS("0","success"),
    FAILURE("1","failure"),
    CLIENT_NOT_EXIST("2","client not exist");

    private String key;
    private String value;

    AuthStatusEnum(String key, String value){
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String getAuthStatusMsg(String key) {
        for (AuthStatusEnum statusEnum: AuthStatusEnum.values()) {
            if (key.equals(statusEnum.getKey())){
                return  statusEnum.getValue();
            }
        }
        return "";
    }
}
