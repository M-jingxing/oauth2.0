package com.zll.enums;

/**
 * Created by Administrator on 2017/4/12.
 */
public enum  LoginStatusEnum {

    SUCCESS("0","success"),
    FAILURE("1","Login failure"),
    PARAM_EMPTY("2","param empty"),
    USER_NOT_EXISTENCE("3","user not existence"),
    PASSWORD_WRONG("4","password wrong"),
    LOGGED_IN("5","logged in"),
    NOT_LOGGED_IN("6","not logged in");



    private String key;
    private String value;

    LoginStatusEnum(String key, String value){
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String getLoginStatusMsg(String key) {
        for (LoginStatusEnum statusEnum:LoginStatusEnum.values()) {
            if (key.equals(statusEnum.getKey())){
                return  statusEnum.getValue();
            }
        }
        return "";
    }
}
