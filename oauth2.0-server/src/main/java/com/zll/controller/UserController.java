package com.zll.controller;

import com.zll.domain.User;
import com.zll.enums.LoginStatusEnum;
import com.zll.enums.RegisterStatusEnum;
import com.zll.service.IUserService;
import com.zll.util.MD5Utils;
import com.zll.util.SessionUtil;
import com.zll.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Administrator on 2017/4/12.
 */
@Controller
@RequestMapping(value = "user")
public class UserController {

    @Autowired
    private IUserService userService;

    /**
     * user register
     * @param user
     * @return
     */
    @RequestMapping(value = "registerUser")
    @ResponseBody
    public ResultVo registerUser(User user){

        if (StringUtils.isEmpty(user.getUserId())){
            return new ResultVo(RegisterStatusEnum.PARAM_EMPTY.getKey(),
                    RegisterStatusEnum.PARAM_EMPTY.getValue());
        }
        String userId = user.getUserId();
        User oldUser = userService.selectByUserId(userId);
        if (oldUser != null){
            return new ResultVo(RegisterStatusEnum.ALREADY_EXIST.getKey(),
                    RegisterStatusEnum.ALREADY_EXIST.getValue());
        }

        Boolean result = userService.insertUserInfo(user);
        if (!result){
            return new ResultVo(RegisterStatusEnum.FAILURE.getKey(),
                    RegisterStatusEnum.FAILURE.getValue());
        }

        User returnUser = userService.selectByUserId(user.getUserId());
        return new ResultVo(returnUser);

    }


    /**
     * user login
     * @param userId
     * @param password
     * @return
     */
    @RequestMapping(value = "loginUser")
    @ResponseBody
    public ResultVo loginUser(@RequestParam("userId") String userId,
                              @RequestParam("password") String password,
                              HttpSession session){

        User user = userService.selectByUserId(userId);
        if (user == null){
            return new ResultVo(LoginStatusEnum.USER_NOT_EXISTENCE.getKey(),
                    LoginStatusEnum.USER_NOT_EXISTENCE.getValue());
        }

        String decodePassword = "";
        try {
             decodePassword = MD5Utils.decode(user.getPassword());
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (!password.equals(decodePassword)){
            return new ResultVo(LoginStatusEnum.PASSWORD_WRONG.getKey(),
                    LoginStatusEnum.PASSWORD_WRONG.getValue());
        }

        SessionUtil.setSessionUser(session,user);

        User returnUser = userService.selectByUserId(userId);
        return new ResultVo(returnUser);
    }

    @RequestMapping(value = "/logoutUser")
    @ResponseBody
    public ResultVo logoutUser(HttpSession session){
        SessionUtil.removeSessionUser(session);
        return new ResultVo(true);
    }

}
