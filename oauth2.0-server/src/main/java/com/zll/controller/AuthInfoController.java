package com.zll.controller;

import com.sun.org.apache.bcel.internal.generic.InstructionConstants;
import com.zll.domain.AuthInfo;
import com.zll.domain.Client;
import com.zll.domain.User;
import com.zll.enums.AuthConstant;
import com.zll.enums.AuthStatusEnum;
import com.zll.enums.FlagEnum;
import com.zll.enums.LoginStatusEnum;
import com.zll.service.IAuthInfoService;
import com.zll.service.IClientService;
import com.zll.service.IUserService;
import com.zll.util.MD5Utils;
import com.zll.util.SessionUtil;
import com.zll.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * Created by Administrator on 2017/4/13.
 */
@Controller
@RequestMapping(value = "oauth")
public class AuthInfoController {

    @Autowired
    private IAuthInfoService authInfoService;
    @Autowired
    private IClientService clientService;
    @Autowired
    private IUserService userService;

    @RequestMapping(value = "/authorize")
    public String authorize(@RequestParam("client_id") String client_id,
                            @RequestParam("redirect_uri") String redirect_uri,
                            @RequestParam("response_type") String response_type,
                            String scope,
                            String state,
                            HttpSession session,
                            Model model){

        Client client = clientService.selectByClientIdAndRedirectUri(client_id,redirect_uri);
        if (client == null){
            return "web_error.html";
        }

        if (!StringUtils.isEmpty(scope)){
            AuthConstant.SCOPE = scope;
        }
        if (!StringUtils.isEmpty(state)){
            AuthConstant.STATE = state;
        }

        SessionUtil.setSessionClientId(session,client_id);
        User user = SessionUtil.getSessionUser(session);

        if (user != null){
            model.addAttribute("user",user);
            return "user_hasLogin_auth.jsp";
        }
        return "user_login_auth.html";
    }

    @RequestMapping(value = "isLogin")
    @ResponseBody
    public ResultVo isLogin(HttpSession session){

        User currentUser = SessionUtil.getSessionUser(session);
        if (currentUser == null) {
            return new ResultVo(LoginStatusEnum.NOT_LOGGED_IN.getKey(),
                    LoginStatusEnum.NOT_LOGGED_IN.getValue());

        }
        return new ResultVo();
    }

    @RequestMapping(value = "isLoginAuthorize")
    public String authorize(HttpSession session){

        String client_id = SessionUtil.getSessionClientId(session);
        String userId = SessionUtil.getSessionUser(session).getUserId();

        System.out.println("client_id:"+client_id);
        System.out.println("userId:"+userId);

        String code = authInfoService.getOldCodeOrNewCode(client_id,userId);

        Boolean isCodeValid = authInfoService.isCodeValid(client_id,userId);

        if (!isCodeValid){
            return "web_error.html";
        }

        Client client = clientService.selectByClientId(client_id);

        String redirectUri ="http://"+client.getRedirectUri() + "?code=" + code;

        System.out.println("uri:"+redirectUri);

        return "redirect:"+redirectUri;
    }

    @RequestMapping(value = "loginAuthorize")
    public String loginAuthorize(@RequestParam("userId") String userId,
                                   @RequestParam("password") String password,
                                   HttpSession session){
        User user = userService.selectByUserId(userId);
        if (user == null){
            return "web_error.html";
        }

        String encryptPassword = MD5Utils.encode(password);
        if (!encryptPassword.equals(user.getPassword())){
            return "web_error.html";
        }

        SessionUtil.setSessionUser(session,user);

        String client_id = SessionUtil.getSessionClientId(session);
        System.out.println("client_id:"+client_id);

        String code = authInfoService.getOldCodeOrNewCode(client_id,userId);

        Boolean isCodeValid = authInfoService.isCodeValid(client_id,userId);

        if (!isCodeValid){
            return "web_error.html";
        }

        Client client = clientService.selectByClientId(client_id);

        String redirectUri ="http://"+client.getRedirectUri() + "?code=" + code;

        System.out.println("uri:"+redirectUri);

        return "redirect:"+redirectUri;
    }


    @RequestMapping(value = "getAccessToken",method = RequestMethod.POST)
    @ResponseBody
    public ResultVo getAccessToken(@RequestParam("grant_type") String grant_type,
                                   @RequestParam("code") String code,
                                   @RequestParam("redirect_uri") String redirect_uri,
                                   @RequestParam("client_id") String client_id,
                                   HttpSession session){

        System.out.println("grant_type:"+grant_type+",code:"+code+",redirect_uri:"+redirect_uri+",client_id:"+client_id);
        if (!grant_type.equals(AuthConstant.GRANDT_TYPE)){
            return new ResultVo(AuthStatusEnum.FAILURE.getKey(),AuthStatusEnum.FAILURE.getValue());
        }

        Client client = clientService.selectByClientIdAndRedirectUri(client_id,redirect_uri);
        AuthInfo authInfoForUserId = authInfoService.selectByClientIdAndCode(client_id,code);
        String userId = authInfoForUserId.getUserId();
        System.out.println("client:"+client);
        if (client == null){
            return new ResultVo(AuthStatusEnum.FAILURE.getKey(),AuthStatusEnum.FAILURE.getValue());
        }

        AuthInfo authInfo = authInfoService.selectByClientIdAdnUserId(client_id,userId);

        if (authInfo == null){
            return new ResultVo(AuthStatusEnum.FAILURE.getKey(),AuthStatusEnum.FAILURE.getValue());
        }

        if (!code.equals(authInfo.getCode()) || !FlagEnum.EFFICACY.getKey().equals(authInfo.getCodeFlag())){
            return new ResultVo(AuthStatusEnum.FAILURE.getKey(),AuthStatusEnum.FAILURE.getValue());
        }

        return new ResultVo(authInfoService.getOldTokenOrNewToken(client_id,userId));
    }


    @RequestMapping(value = "getUserInfoByToken")
    @ResponseBody
    public ResultVo getUserInfoByToken(@RequestParam("access_token") String access_token){
        AuthInfo authInfo = authInfoService.selectByAccessToken(access_token);
        if (authInfo == null){
            return new ResultVo(AuthStatusEnum.FAILURE.getKey(),AuthStatusEnum.FAILURE.getValue());
        }

        String userId = authInfo.getUserId();

        User user = userService.selectByUserId(userId);

        return new ResultVo(user);
    }



}
