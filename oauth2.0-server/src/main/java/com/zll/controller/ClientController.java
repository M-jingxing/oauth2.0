package com.zll.controller;

import com.alibaba.fastjson.JSONObject;
import com.zll.domain.Client;
import com.zll.enums.RegisterStatusEnum;
import com.zll.service.IClientService;
import com.zll.util.HttpClientUtil;
import com.zll.util.IPUtil;
import com.zll.vo.ResultVo;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/4/12.
 */
@Controller
@RequestMapping(value = "client")
public class ClientController {


    @Autowired
    private IClientService clientService;

    /**
     * client register
     * @param redirectUri
     * @return
     */
    @RequestMapping(value = "registerClient",method = RequestMethod.POST)
    @ResponseBody
    public ResultVo registerClient(@RequestParam("redirectUri") String redirectUri){
        if (StringUtils.isEmpty(redirectUri)){
            return new ResultVo(RegisterStatusEnum.PARAM_EMPTY.getKey(),
                    RegisterStatusEnum.PARAM_EMPTY.getValue());
        }
        //judge the client weather inserted
        Client oldClient = clientService.selectByRedirectUri(redirectUri);
        if (oldClient != null){
            return new ResultVo(RegisterStatusEnum.ALREADY_EXIST.getKey(),
                    RegisterStatusEnum.ALREADY_EXIST.getValue());
        }

        Client client = new Client();
        client.setRedirectUri(redirectUri);

        boolean result =  clientService.insertClientInfo(client);
        if (!result){
            return new ResultVo(RegisterStatusEnum.FAILURE.getKey(),
                    RegisterStatusEnum.FAILURE.getValue());
        }
        Client returnClient = clientService.selectByRedirectUri(client.getRedirectUri());

        String serverId = IPUtil.getMyIp() + ":8080";
        List<NameValuePair> postParam= new ArrayList<NameValuePair>();
        postParam.add(new BasicNameValuePair("clientId",returnClient.getClientId()));
        postParam.add(new BasicNameValuePair("redirectUri",returnClient.getRedirectUri()));
        postParam.add(new BasicNameValuePair("serverId",serverId));
        String getClientUri = redirectUri.split("/")[0];
        String clientUri = "http://"+getClientUri + "/client/getClientInfo.do";

        HttpClientUtil.httpPost(clientUri,postParam);

        return new ResultVo(returnClient);
    }

    /**
     * getClientInfoByUri
     * @param redirectUri
     * @return
     */
    @RequestMapping(value = "getClientInfoByUri")
    @ResponseBody
    public ResultVo getClientInfoByUri(@RequestParam("redirectUri") String redirectUri){
        Client client = clientService.selectByRedirectUri(redirectUri);
        return new ResultVo(client);
    }

}
