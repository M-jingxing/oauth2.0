( function() {
    // initEvent
    // 注销按钮点击事件
    $("#user_logout_btn").on("click", function () {
        $.ajax({
            type: "post",
            url: "/user/logoutUser.do",
            dataType: "json",
            success: function(data){

                if(!data.success){
                    alert(data.errorMsg);
                    return;
                }

                location.href = "user_login.html";
            }
        });
    });
    
    
}());