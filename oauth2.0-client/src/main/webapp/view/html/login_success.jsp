<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>客户端登录成功</title>

    <!-- Bootstrap core CSS -->
    <link href="/view/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/view/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/view/css/landing-page.css" rel="stylesheet">

    <link href="/view/css/style.css" rel="stylesheet" type="text/css">

</head>

<body>
<!--登录成功显示-->
<div class="loginSuccess">
    <h1>${user.userName}<span class="loginSuccessTip">，登录成功！</span></h1>
    <table class="lead">
        <tbody>
        <tr>
            <td colspan="2" class="warning">以下是您授权的个人信息</td>
        </tr>
        <tr>
            <td>账号：</td>
            <td id="register_user_id">${user.userId}</td>
        </tr>
        <tr>
            <td>性别：</td>
            <td id="register_user_sex"><c:if test="${user.sex == 1}">男</c:if>
                                       <c:if test="${user.sex == 2}">女</c:if></td>
        </tr>
        <tr>
            <td>学校：</td>
            <td id="register_user_school">${user.school}</td>
        </tr>
        <tr>
            <td>年级：</td>
            <td id="register_user_grade">${user.grade}</td>
        </tr>
        <tr>
            <td colspan="2">
                <button type="button" id="user_logout_btn" class="btn btn-primary btn-block-half btn-large" style="margin-left: 141px; font-family: cursive;">Log&nbsp;out&nbsp;<i
                        class="fa fa-arrow-right fa-fw"></i></button>
            </td>
        </tr>
        </tbody>
    </table>
</div>

<script src="/view/js/jquery/jquery-1.11.1.min.js"></script>
<script src="/view/js/user_login.js"></script>
</body>
</html>
