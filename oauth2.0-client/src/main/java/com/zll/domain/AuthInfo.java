package com.zll.domain;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/4/12.
 */
public class AuthInfo implements Serializable {
    private Long id;
    private String clientId;
    private String userId;
    private String code;
    private String codeLimit;
    private String accessToken;
    private String tokenLimit;
    private String refreshToken;
    private String refreshLimit;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeLimit() {
        return codeLimit;
    }

    public void setCodeLimit(String codeLimit) {
        this.codeLimit = codeLimit;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenLimit() {
        return tokenLimit;
    }

    public void setTokenLimit(String tokenLimit) {
        this.tokenLimit = tokenLimit;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getRefreshLimit() {
        return refreshLimit;
    }

    public void setRefreshLimit(String refreshLimit) {
        this.refreshLimit = refreshLimit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthInfo authInfo = (AuthInfo) o;

        if (id != null ? !id.equals(authInfo.id) : authInfo.id != null) return false;
        if (clientId != null ? !clientId.equals(authInfo.clientId) : authInfo.clientId != null) return false;
        if (userId != null ? !userId.equals(authInfo.userId) : authInfo.userId != null) return false;
        if (code != null ? !code.equals(authInfo.code) : authInfo.code != null) return false;
        if (codeLimit != null ? !codeLimit.equals(authInfo.codeLimit) : authInfo.codeLimit != null) return false;
        if (accessToken != null ? !accessToken.equals(authInfo.accessToken) : authInfo.accessToken != null)
            return false;
        if (tokenLimit != null ? !tokenLimit.equals(authInfo.tokenLimit) : authInfo.tokenLimit != null) return false;
        if (refreshToken != null ? !refreshToken.equals(authInfo.refreshToken) : authInfo.refreshToken != null)
            return false;
        return refreshLimit != null ? refreshLimit.equals(authInfo.refreshLimit) : authInfo.refreshLimit == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (clientId != null ? clientId.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (codeLimit != null ? codeLimit.hashCode() : 0);
        result = 31 * result + (accessToken != null ? accessToken.hashCode() : 0);
        result = 31 * result + (tokenLimit != null ? tokenLimit.hashCode() : 0);
        result = 31 * result + (refreshToken != null ? refreshToken.hashCode() : 0);
        result = 31 * result + (refreshLimit != null ? refreshLimit.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AuthInfo{" +
                "id=" + id +
                ", clientId='" + clientId + '\'' +
                ", userId='" + userId + '\'' +
                ", code='" + code + '\'' +
                ", codeLimit='" + codeLimit + '\'' +
                ", accessToken='" + accessToken + '\'' +
                ", tokenLimit='" + tokenLimit + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                ", refreshLimit='" + refreshLimit + '\'' +
                '}';
    }
}
