package com.zll.domain;

/**
 * Created by Administrator on 2017/3/10.
 */
public class ResultVo<T> {

    private String errorCode = "0";
    private String errorMsg = "成功";
    private boolean success = true;
    private T data;

    public ResultVo() {
    }

    public ResultVo(T t){
        this.data = t;
    }

    public ResultVo(String errorCode, String errorMsg){
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
        this.success = false;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
