package com.zll.controller;

import com.alibaba.fastjson.JSONObject;
import com.zll.domain.Client;
import com.zll.domain.ResultVo;
import com.zll.util.Constant;
import com.zll.util.HttpClientUtil;
import com.zll.util.SaveInfoFileUtil;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/4/13.
 */
@Controller
@RequestMapping(value = "client")
public class AuthController {

    @RequestMapping(value = "getCode")
    public String getCode(@RequestParam("code") String code, Model model, HttpServletRequest request) throws IOException {
        System.out.println("code:" + code);

        String url = /*request.getScheme() +"://" +*/ request.getServerName()

                + ":" + request.getServerPort()

                + request.getServletPath();

        System.out.println(url);

        List<NameValuePair> postParam = new ArrayList<NameValuePair>();
        postParam.add(new BasicNameValuePair("grant_type", "authorization_code"));
        postParam.add(new BasicNameValuePair("code", code));
        postParam.add(new BasicNameValuePair("redirect_uri", url));
        postParam.add(new BasicNameValuePair("client_id", Constant.getClientId(request)));

        String requestTokenUri = Constant.getRequestTokenUri(request);

        JSONObject tokenJson = HttpClientUtil.httpPost(requestTokenUri, postParam);
        System.out.println("tokenJson:" + tokenJson);
        String token = tokenJson.getString("data");
        System.out.println("token:" + token);

        if (token == null) {
            return "test.jsp";
        }

        String requestUserInfoUri = Constant.getRequestUserInfoUri(request);

        List<NameValuePair> userInfoPostParam = new ArrayList<NameValuePair>();
        userInfoPostParam.add(new BasicNameValuePair("access_token", token));
        JSONObject resultUserInfoJson = HttpClientUtil.httpPost(requestUserInfoUri, userInfoPostParam);
        JSONObject userInfo = (JSONObject) resultUserInfoJson.get("data");

        System.out.println("userInfo:" + userInfo.toString());

        model.addAttribute("user", userInfo);
        return "login_success.jsp";
    }

    @RequestMapping(value = "/getClientInfo",method = RequestMethod.POST)
    @ResponseBody
    public ResultVo getClientInfo(@RequestParam("clientId") String clientId,
                                  @RequestParam("redirectUri") String redirectUri,
                                  @RequestParam("serverId") String serverId,
                                  HttpServletRequest request) throws IOException {
        System.out.println("clientId:"+clientId);
        SaveInfoFileUtil.createAndSave(clientId,redirectUri,serverId,request);
        return new ResultVo(true);
    }

    @RequestMapping(value = "/redirectToAuthUri")
    @ResponseBody
    public ResultVo redirectToAuthUri(HttpServletRequest request) throws IOException {
        String uri = "http://" + Constant.getServerIp(request) + "/oauth/authorize.do?client_id=" + Constant.getClientId(request) + "&redirect_uri=" + Constant.getRedirectUri(request) + "&response_type=code";
        System.out.println("uri:"+uri);
        return new ResultVo(uri);
    }


}
