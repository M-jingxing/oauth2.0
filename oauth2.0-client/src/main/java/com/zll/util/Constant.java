package com.zll.util;

import com.zll.domain.ClientInfo;
import org.apache.http.protocol.HTTP;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by Administrator on 2017/4/16.
 */
public class Constant {

    public static String getServerIp(HttpServletRequest request) throws IOException{
        ClientInfo clientInfo = SaveInfoFileUtil.readAndShow(request);
        return clientInfo.getServerId();
    }
    public static String getClientId(HttpServletRequest request) throws IOException{
        ClientInfo clientInfo = SaveInfoFileUtil.readAndShow(request);
        return clientInfo.getClientId();
    }
    public static String getRedirectUri(HttpServletRequest request) throws IOException{
        ClientInfo clientInfo = SaveInfoFileUtil.readAndShow(request);
        return clientInfo.getRedirectUri();
    }

    public static String getRequestTokenUri(HttpServletRequest request) throws IOException {
        ClientInfo clientInfo = SaveInfoFileUtil.readAndShow(request);
        String requestTokenUri = "http://"+clientInfo.getServerId()+"/oauth/getAccessToken.do";
        return requestTokenUri;
    }
    public static String getRequestUserInfoUri(HttpServletRequest request) throws IOException {
        ClientInfo clientInfo = SaveInfoFileUtil.readAndShow(request);
        String requestUserInfoUri = "http://"+clientInfo.getServerId()+"/oauth/getUserInfoByToken.do";
        return requestUserInfoUri;
    }
}
