package com.zll.util;

import com.zll.domain.ClientInfo;

import javax.servlet.http.HttpServletRequest;
import java.io.*;


/**
 * Created by ZLL on 2017/5/7.
 */
public class SaveInfoFileUtil {
    // 文件名可随意指定，你可以用文本编辑器打开这个文件（注意，记事本无法处理换行）
    static String filename ="clientInfo.data";

    public static void main(String[] args) throws Exception {
        // 将这个程序运行两遍。
        // 第一遍它会创建一些 Person 对象并保存到文件；
        // 第二遍它会从文件中读取对象数据并显示出来。

         //   createAndSave("111qwre","222b","333c");
        //    System.out.println(readAndShow().getClientId());
       //   System.out.println(System.getProperty("user.dir"));


    }

    // 生成并保存 clientInfo 对象
    public static void createAndSave(String clientId,String redirectUri, String serverId,HttpServletRequest request) throws IOException {
        ClientInfo clientInfo = createClient(clientId,redirectUri,serverId);
        saveClientInfo(clientInfo,request);
    }

    // 读取并显示 clientInfo 对象
    public static ClientInfo readAndShow(HttpServletRequest request) throws IOException {
        ClientInfo clientInfo = readPersons(request);
        return clientInfo;
    }

    // 创建要保存的 clientInfo 对象
    private static ClientInfo createClient(String clientId,String redirectUri, String serverId) {
        ClientInfo clientInfo = new ClientInfo();
        clientInfo.setClientId(clientId);
        clientInfo.setRedirectUri(redirectUri);
        clientInfo.setServerId(serverId);
        return clientInfo;
    }

    private static void saveClientInfo(ClientInfo clientInfo,HttpServletRequest request) throws IOException {

        // 生成文件内容
        String data = getClientInfoString(clientInfo);
        System.out.println(request.getSession().getServletContext().getRealPath("/")+filename);
        // 保存文件内容
        FileWriter writer = new FileWriter(request.getSession().getServletContext().getRealPath("/")+filename);
        writer.write(data);
        writer.close();
        System.out.println("对象保存完毕。");
    }

    private static String getClientInfoString(ClientInfo clientInfo) {
        return clientInfo.getClientId() + "/t" + clientInfo.getRedirectUri()+ "/t" + clientInfo.getServerId();
    }

    private static ClientInfo readPersons(HttpServletRequest request) throws IOException {
        ClientInfo clientInfo = new ClientInfo();
        BufferedReader reader = new BufferedReader(new FileReader(request.getSession().getServletContext().getRealPath("/")+ filename));
        String line;
        while ((line = reader.readLine()) != null) {
           clientInfo = getClientInfoFromString(line);
        }

        return clientInfo;
    }

    private static ClientInfo getClientInfoFromString(String line) {
        String[] parts = line.split("/t");  // 获取被分隔的三个部分
        ClientInfo clientInfo = new ClientInfo();
        clientInfo.setClientId(parts[0]);
        clientInfo.setRedirectUri(parts[1]);
        clientInfo.setServerId(parts[2]);
        return clientInfo;
    }

    private static void showClientInfo(ClientInfo clientInfo) {

            System.out.println(clientInfo.getClientId() + ", " +
                    clientInfo.getRedirectUri()+ ", " +
                    clientInfo.getServerId());

    }

}
